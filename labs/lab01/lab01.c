    #include "pico/stdlib.h"

/**
 * @brief Toggle the state of the LED.
 *
 * This function toggles the state of the LED connected to the specified pin
 * and then sleeps for the given delay period.
 *
 * @param pin The GPIO pin number to which the LED is connected.
 * @param delay_ms The delay period in milliseconds.
 */
void toggle_led(int pin, int delay_ms) {
    // Toggle the LED on and then sleep for the delay period
    gpio_put(pin, 1);
    sleep_ms(delay_ms);

    // Toggle the LED off and then sleep for the delay period
    gpio_put(pin, 0);
    sleep_ms(delay_ms);
}

/**
 * @brief Main function of the blink example.
 *
 * This function initializes the LED pin and continuously toggles the LED state.
 * It runs an infinite loop, and the LED keeps flashing until the program is terminated.
 *
 * @return int Application return code (zero for success).
 */
int main() {
    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    while (true) {
        toggle_led(LED_PIN, LED_DELAY);
    }

    // Should never get here due to the infinite while-loop.
    return 0;
}

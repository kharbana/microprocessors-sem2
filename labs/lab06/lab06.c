#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/time.h" 
#include "pico/multicore.h"
#include "pico/stdlib.h"

void core1_entry();
float single_precision();
double double_precision();

int main() {
    stdio_init_all();
    
    // Measure and print total runtime on core 0 only
    absolute_time_t start_core0 = get_absolute_time();
    
    double double_pi_core0 = double_precision();
    float single_pi_core0 = single_precision();
    
    absolute_time_t end_core0 = get_absolute_time();
    int64_t total_runtime_core0 = absolute_time_diff_us(start_core0, end_core0);
    printf("Total runtime on core 0 only: %lld microseconds\n", total_runtime_core0);
    
    // Launch core 1 and measure total runtime on both cores
    multicore_launch_core1(core1_entry);
    absolute_time_t start_both_cores = get_absolute_time();
    
    // Execute double_precision on core 0
    double_pi_core0 = double_precision();
    // Execute single_precision on core 1 by pushing a dummy value to start execution
    multicore_fifo_push_blocking(1); // Signal core 1 to start single_precision
    
    // Wait for core 1 to finish
    multicore_fifo_pop_blocking(); // Wait for core 1 to signal completion
    
    absolute_time_t end_both_cores = get_absolute_time();
    int64_t total_runtime_both_cores = absolute_time_diff_us(start_both_cores, end_both_cores);
    printf("Total runtime on both cores: %lld microseconds\n", total_runtime_both_cores);
    
    return 0;
}

void core1_entry() {
    // Wait for signal from core 0 to start single_precision
    multicore_fifo_pop_blocking();
    
    // Execute single_precision
    float single_pi_core1 = single_precision();
    
    // Signal core 0 that execution is complete
    multicore_fifo_push_blocking(1);
}

float single_precision() {
    float calc_pi = 1.0;
    for (float n = 1.0; n <= 100000.0; n++) {
        calc_pi = calc_pi * ((4*n*n) / ((2*n - 1) * (2*n + 1)));
    }
    return 2 * calc_pi;
}

double double_precision() {
    double calc_pi = 1.0;
    for (double n = 1.0; n <= 100000.0; n++) {
        calc_pi = calc_pi * ((4*n*n) / ((2*n - 1) * (2*n + 1)));
    }
    return 2 * calc_pi;
}
/**
 * @file main.c
 * @brief This program measures the performance of single and double precision calculations of Pi.
 *        It can run these calculations on one or two cores of the Raspberry Pi Pico, with the
 *        option to enable or disable the XIP cache.
 *
 * @details The program includes functions to measure the performance of calculating Pi using
 *          single and double precision arithmetic. It also includes functions to enable or
 *          disable the XIP cache, which can affect performance. The main function runs the
 *          performance measurements in four different configurations: single core with cache
 *          enabled, single core with cache disabled, both cores with cache enabled, and both
 *          cores with cache disabled.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/time.h" 
#include "pico/multicore.h"
#include "pico/stdlib.h"

const int ITERATIONS = 100000; ///< Number of iterations for Pi calculation.

// Assume these functions are defined elsewhere
extern bool get_xip_cache_en(); ///< Function to check if XIP cache is enabled.
extern bool set_xip_cache_en(bool cache_en); ///< Function to enable or disable XIP cache.

void core1_entry(); ///< Entry function for core 1.
float single_precision(int iter); ///< Function to calculate Pi using single precision.
double double_precision(int iter); ///< Function to calculate Pi using double precision.
void measure_performance(bool use_two_cores, bool cache_enabled); ///< Function to measure performance of Pi calculations.

/**
 * @brief Main function that initializes the stdio and runs performance measurements.
 *
 * @return int Returns 0 upon successful completion.
 */
int main() {
    stdio_init_all();
    
    // Single core with cache enabled
    printf("Single core with cache enabled:\n");
    measure_performance(false, true);
    
    // Single core with cache disabled
    printf("Single core with cache disabled:\n");
    measure_performance(false, false);
    
    // Both cores with cache enabled
    printf("Both cores with cache enabled:\n");
    measure_performance(true, true);
    
    // Both cores with cache disabled
    printf("Both cores with cache disabled:\n");
    measure_performance(true, false);
    
    return 0;
}

/**
 * @brief Measures the performance of single and double precision Pi calculations.
 *
 * @param use_two_cores Indicates whether to use one core or both cores for the calculations.
 * @param cache_enabled Indicates whether the XIP cache is enabled or disabled.
 */
void measure_performance(bool use_two_cores, bool cache_enabled) {
    // Set the cache as per the value of cache_enabled
    set_xip_cache_en(cache_enabled);

    absolute_time_t start_time, end_time;
    int64_t single_precision_time, double_precision_time, total_time;

    if (use_two_cores) {
        // Engage core 1 to run single_precision
        multicore_launch_core1(core1_entry);

        // Start timing for single_precision
        start_time = get_absolute_time();
        multicore_fifo_push_blocking((uintptr_t)single_precision);
        multicore_fifo_push_blocking(ITERATIONS); // Example iteration count
        single_precision_time = absolute_time_diff_us(start_time, get_absolute_time());

        // Run double_precision on core 0
        start_time = get_absolute_time();
        double_precision_time = absolute_time_diff_us(start_time, get_absolute_time());

        // Calculate total time
        total_time = single_precision_time + double_precision_time;

        // Print the time taken
        printf("Time taken by single_precision on core 1: %lld us\n", single_precision_time);
        printf("Time taken by double_precision on core 0: %lld us\n", double_precision_time);
    } else {
        // Run single_precision on core 0
        start_time = get_absolute_time();
        single_precision(ITERATIONS); // Example iteration count
        single_precision_time = absolute_time_diff_us(start_time, get_absolute_time());

        // Run double_precision on core 0
        start_time = get_absolute_time();
        double_precision(ITERATIONS); // Example iteration count
        double_precision_time = absolute_time_diff_us(start_time, get_absolute_time());

        // Calculate total time
        total_time = single_precision_time + double_precision_time;

        // Print the time taken
        printf("Time taken by single_precision on core 0: %lld us\n", single_precision_time);
        printf("Time taken by double_precision on core 0: %lld us\n", double_precision_time);
    }

    // Print the total time taken
    printf("Total time taken: %lld us\n", total_time);
}

/**
 * @brief Entry function for core 1 that continuously waits for functions to execute and parameters to process.
 */
void core1_entry() {
    while (1) {
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

/**
 * @brief Calculates Pi using single precision arithmetic.
 *
 * @param iter Number of iterations to use in the calculation.
 * @return float The calculated value of Pi.
 */
float single_precision(int iter) {
    float calc_pi = 1.0;
    for (float n = 1.0; n <= iter; n++) {
        calc_pi = calc_pi * ((4*n*n) / ((2*n - 1) * (2*n + 1)));
    }
    return 2 * calc_pi;
}

/**
 * @brief Calculates Pi using double precision arithmetic.
 *
 * @param iter Number of iterations to use in the calculation.
 * @return double The calculated value of Pi.
 */
double double_precision(int iter) {
    double calc_pi = 1.0;
    for (double n = 1.0; n <= iter; n++) {
        calc_pi = calc_pi * ((4*n*n) / ((2*n - 1) * (2*n + 1)));
    }
    return 2 * calc_pi;
}

/**
 * @brief Checks if the XIP cache is enabled by reading the first bit of the XIP_CTRL_BASE address.
 *
 * @return bool True if the XIP cache is enabled, false otherwise.
 */
bool get_xip_cache_en(){
    const uint8_t *flash_target_contents = (const uint8_t *) (XIP_CTRL_BASE);  // Read flash memory address from XIP_CTRL_BASE
    // Reading first bit of address
    if(flash_target_contents[0] == 1){
        return true;
    }
    return false;
}

/**
 * @brief Enables or disables the XIP cache by writing to the XIP_CTRL_BASE address.
 *
 * @param cache_en The desired state of the XIP cache.
 * @return bool Always returns true.
 */
bool set_xip_cache_en(bool cache_en){
    bool *q = (bool*) XIP_CTRL_BASE;  // Read flash memory address from XIP_CTRL_BASE
    *q = cache_en; // Write the value of cache_en to XIP_CTRL_BASE EN bit
    return true;
}
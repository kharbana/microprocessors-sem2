//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/float.h"
#include "pico/double.h"

//set value of Pi
const float pi = 3.14159265359;

/**
 * @brief Calculates the approximation of Pi using single precision floating-point arithmetic.
 *
 * @return Calculated float value of Pi.
 */
float single_precision() {
  float calc_pi = 1.0;
  for (float n = 1.0; n <= 100000.0; n++) {
    calc_pi = calc_pi * ((4*n*n) / ((2*n - 1) * (2*n + 1)));
  }
  return 2 * calc_pi;
}

/**
 * @brief Calculates the approximation of Pi using double precision floating-point arithmetic.
 *
 * @return Calculatd double value of Pi.
 */
double double_precision() {
  double calc_pi = 1.0;
  for (double n = 1.0; n <= 100000.0; n++) {
    calc_pi = calc_pi * ((4*n*n) / ((2*n - 1) * (2*n + 1)));
  }
  return 2 * calc_pi;
}

/**
 * @brief The main function of the program.
 *
 * @details Initializes IO if not running on Wokwi RP2040 emulator, calculates
 *          single and double precision Pi values, computes their errors, and
 *          prints the results to the console.
 *
 * @return int The application return code (zero for success).
 */
int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    float single_pi = single_precision();
    double double_pi = double_precision();
    float single_error = (pi - single_pi);
    double double_error = (pi - double_pi);
    // Print a console message to inform user what's going on.
    printf("Single Precision Representation Pi: %f\n", single_pi);
    printf("Single Precision Error: %f\n", single_error);
    printf("Single Precision Percentage Error: %f\n", single_error * 100 / pi);
    printf("Double Precision Representation Pi: %lf\n", double_pi);
    printf("Double Precision Error: %f\n", double_error);
    printf("Double Precision Percentage Error: %lf\n", double_error * 100 / pi);

    // Returning zero indicates everything went okay.
    return 0;
}
